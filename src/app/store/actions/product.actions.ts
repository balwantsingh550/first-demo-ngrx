import { createAction, props } from '@ngrx/store';
import { Products } from 'src/app/core/contract/products';
import { ProductState } from '../reducers/product.reducer';

export const createProductAction = (actionType: string) =>
  createAction(actionType, props<{ products: Products }>());

export const getProudct = createAction('[Product] GET_PRODUCT');

export const getProductSuccess = createAction(
  '[Product] GET_PRODUCT_SUCCESS',
  props<{ products: Products[] }>()
);

export const getProductError = createAction(
  '[Products] GET_PRODUCT_ERROR',
  props<{ error: any }>()
);

export const addTocart = createAction(
  '[Products] ADD_TO_CART',
  props<{ id: number }>()
);
