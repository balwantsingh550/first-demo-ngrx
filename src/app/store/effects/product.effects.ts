import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import * as ProductAction from '../actions';
import { ProductsService } from '../services/products.service';
import { catchError, concatMap, map, switchMap } from 'rxjs/operators';

@Injectable()
export class ProductEffects {
  getHeroes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ProductAction.getProudct),
      switchMap(() =>
        this.productDataService.getProduct().pipe(
          map((products) => ProductAction.getProductSuccess({ products })),
          catchError((error) => of(ProductAction.getProductError({ error })))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private productDataService: ProductsService
  ) {}
}
