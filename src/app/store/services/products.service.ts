import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, delay } from 'rxjs/operators';
import { Products } from 'src/app/core/contract/products';
import { products } from 'src/app/core/schema/products';
import { DataServiceError } from '../config';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  constructor(private http: HttpClient) {}

  getProduct(): Observable<Products[]> {
    let data;
    return data = of(products).pipe(
      delay(3000),
      catchError(this.handleError())
    );;
  }


  private handleError<T>(requestData?: T) {
    return (res: any) => {
      const error = new DataServiceError(res.error, requestData);
      console.error(error);
      // return new ErrorObservable(error);
      return throwError(error);
    };
  }
}
