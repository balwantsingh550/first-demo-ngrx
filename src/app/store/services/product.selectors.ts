import { Injectable } from '@angular/core';
import { createFeatureSelector, createSelector, Store } from '@ngrx/store';
import { EntityState } from '../reducers';
import { ProductState } from '../reducers/product.reducer';

const getEntityState = createFeatureSelector<EntityState>('entityCache');

const getProductState = createSelector(
  getEntityState,
  (state: EntityState) => state.products
);

const getAllProducts = createSelector(
  getProductState,
  (state: ProductState) => state.products
);

@Injectable()
export class ProductSelectors {
  constructor(private store: Store<EntityState>) {}
  // selectors$
  products$ = this.store.select(getAllProducts);
}
