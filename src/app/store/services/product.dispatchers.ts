import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { EntityState } from '../reducers';
import * as ProductAction from '../actions';

@Injectable()
export class ProductDispatchers {
  constructor(private store: Store<EntityState>) {}

  getProducts() {
    this.store.dispatch(ProductAction.getProudct());
  }

  addTocart(id: number) {
    this.store.dispatch(ProductAction.addTocart({ id }));
  }
}
