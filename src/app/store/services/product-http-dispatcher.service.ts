import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { EntityState } from '../reducers';
import { ProductsService } from './products.service';
import * as ProductAction from '../actions';

@Injectable({
  providedIn: 'root',
})
export class ProductHttpDispatcherService {
  getHeroes() {
    this.productservice.getProduct().subscribe(
      (products) =>
        this.dispatch(ProductAction.getProductSuccess({ products })),
      (error) => this.dispatch(ProductAction.getProductError(error))
    );
  }

  constructor(
    private store: Store<EntityState>,
    private productservice: ProductsService
  ) {}

  private dispatch = (action: Action) => this.store.dispatch(action);
}
