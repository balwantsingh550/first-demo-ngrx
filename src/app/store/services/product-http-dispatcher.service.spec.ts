import { TestBed } from '@angular/core/testing';

import { ProductHttpDispatcherService } from './product-http-dispatcher.service';

describe('ProductHttpDispatcherService', () => {
  let service: ProductHttpDispatcherService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductHttpDispatcherService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
