import { ActionReducerMap } from '@ngrx/store';
import * as fromProduct from './product.reducer';

export interface EntityState {
  products: fromProduct.ProductState;
}

export const reducers: ActionReducerMap<EntityState> = {
  products: fromProduct.reducer,
  // here is where i put other reducers, when i have them
};
