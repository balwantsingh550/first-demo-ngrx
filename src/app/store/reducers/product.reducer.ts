import { Action, createReducer, on } from '@ngrx/store';
import { Products } from 'src/app/core/contract/products';
import * as ProductAction from '../actions';

export interface ProductState {
  products: Products[];
  loading: boolean;
  error: boolean;
  cart: number;
}

export const initialState: ProductState = {
  products: [],
  loading: false,
  error: false,
  cart: 0,
};

function modifyProductState(
  productState: ProductState,
  producChanges: Partial<Products>
): ProductState {
  return {
    ...productState,
    loading: false,
    products: productState.products.map((h) => {
      if (h.id === producChanges.id) {
        return { ...h, ...producChanges };
      } else {
        return h;
      }
    }),
  };
}

const productReducer = createReducer(
  initialState,
  on(ProductAction.getProudct, (state) => ({ ...state, loading: true })),
  on(ProductAction.addTocart, (state) => ({ ...state, cart: state.cart + 1 })),
  on(ProductAction.getProductError, (state) => ({ ...state, loading: false })),
  on(ProductAction.getProductSuccess, (state, { products }) => ({
    ...state,
    loading: false,
    products,
  }))
);

export function reducer(state: ProductState | undefined, action: Action) {
  return productReducer(state, action);
}
