export * from './actions';
export * from './effects';
export * from './reducers';
export * from './services';

import {
  ProductDispatchers,
  ProductsService,
  ProductSelectors,
  ProductHttpDispatcherService
} from './services';

export const services = [
    ProductDispatchers,
    ProductsService,
    ProductSelectors,
    ProductHttpDispatcherService
];
