import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Products } from 'src/app/core/contract/products';
import { products } from 'src/app/core/schema/products';
import { ProductDispatchers, ProductSelectors } from 'src/app/store';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
  products: any;
  products$: Observable<any[]>;
  constructor(
    private productDispature: ProductDispatchers,
    private productSelector: ProductSelectors
  ) {
    this.products$ = this.productSelector.products$;
  }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.productDispature.getProducts();
  }

  addTocart(id: number) {
    this.productDispature.addTocart(id);
  }
}
