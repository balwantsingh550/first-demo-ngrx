export interface Products {
  id: number;
  name: string;
  image: string;
  desc: string;
  price: string;
}
