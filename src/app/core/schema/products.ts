import { Products } from '../contract/products';

export const products: Products[] = [
  {
    id: 1,
    name: 'ORANGE',
    image:
      'https://sc01.alicdn.com/kf/UT8.CaXX2NXXXagOFbXC/fresh-navel-oranges.jpg',
    desc: 'A bag of delicious oranges!',
    price: '$4.99',
  },
  {
    id: 2,
    name: 'APPLE',
    image: 'https://newenglandapples.files.wordpress.com/2014/10/img_5595.jpg',
    desc: 'A bag of delicious apples!',
    price: '$4.99',
  },
  {
    id: 3,
    name: 'PASSIONFRUIT',
    image:
      'https://sc01.alicdn.com/kf/UT8ovSIXQNaXXagOFbXt/Fresh-Passion-Fruit-with-Best-Price-and.jpg',
    desc: 'A bag of delicious passionfruit!',
    price: '$4.99',
  },
  {
    id: 4,
    name: 'PINEAPPLE',
    image:
      'http://www.foodmatters.com/media/images/articles/16-powerful-reasons-to-eat-pineapple.jpg',
    desc: 'A bag of delicious pineapples!',
    price: '$4.99',
  },
  {
    id: 5,
    name: 'MANGO',
    image: 'http://membrillo.com.au/wp-content/uploads/2016/11/bg-mango-01.jpg',
    desc: 'A bag of delicious mangos!',
    price: '$4.99',
  },
  {
    id: 6,
    name: 'COCONUT',
    image:
      'http://ell.h-cdn.co/assets/16/27/980x490/landscape-1467750721-gettyimages-146896572.jpg',
    desc: 'A bag of delicious coconuts!!',
    price: '$4.99',
  },
];
